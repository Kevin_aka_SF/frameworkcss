# Documentation framework CSS #

## Auteur 

Kévin GUILLAUME - Nicolas CHAMPION

[Lien vers le framework](https://webetu.iutnc.univ-lorraine.fr/~guillau62u/HtmlAvancee/Framework/2.html) (Héberger sur webetu) 

[Lien vers le dépot bitbucket](https://bitbucket.org/Kevin_aka_SF/frameworkcss)

## Les fonctionnalitées ##

###1. Les titres ###

### 2. Les sous-titres ###

### 3. Les paragraphes ###

### 4. Les balises de texte inline ###

    *Mark, Del, Ins, Strong,  Em,  Abbr,   U*

### 5. Les balises de citations ###

    *Q, Blockquote, Code, Pre, Pre personnalisée, kdb*

### 6. Les listes UL / LI ###

### 7. L'alignement et tranformation de texte ###

### 8. Les tableaux ###

### 9. Les boutons ###

### 10. Les alertes ###

### 11. Jumbotron ###

### 12. Fil d'ariane ###

### 13. Pagination ###

### 14. Autres ###

    La barre de menu horizontale inactive. Le couleur bleu indique que le lien est actif. La couleur rouge clair indique que le lien est inactif et bloqué.

    La barre de menu horizontale est fixé au navigateur. Elle change de taille en fonction du scroll de la page.
    
    Le framework est responsive et utilise un système de grille sur 12 colonnes. 

    Le framework utilise du javascript pour la modélisation des exemples en HTML et JS dans les section *Balise inline*, *balise de citation*

## Utilisation

    Naviguation simple avec une barre verticale qui permet d'accéder plus rapidement au éléments de la page. La barre de menu horizontale est inactif car elle permet de montrer un exemple de barre de menu.
    Afin d'utiliser correctement le framework, récuperez seulement les dossiers css, js img ainsi que le fichier 2.html